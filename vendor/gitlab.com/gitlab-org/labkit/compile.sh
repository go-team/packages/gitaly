#!/usr/bin/env sh

set -eux
IFS="$(printf '\n\t')"

dir=$(cd -- "$(dirname -- "$0")" && pwd)

cd "${dir}"

for build_tags in \
  "" \
  "tracer_static tracer_static_jaeger" \
  "tracer_static tracer_static_lightstep" \
  "tracer_static tracer_static_datadog" \
  "tracer_static tracer_static_stackdriver" \
  "tracer_static tracer_static_jaeger tracer_static_lightstep tracer_static_datadog tracer_static_stackdriver" \
  "continuous_profiler_stackdriver" \
  ; do
  (
    set -x;

    go build \
    -tags "${build_tags}" \
    ./...
  )
done
