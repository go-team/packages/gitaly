package impl

import (
	"github.com/lightstep/lightstep-tracer-go"
	"github.com/opentracing/opentracing-go"
	"github.com/uber/jaeger-client-go"
)

// IsSampled returns the sampling status (true/false) of a span. Most
// implementations support checking this status. Unfortunately, we hide the
// tracing implementations behind unified opentracing interfaces. The single
// input is an opentracing.Span interface. As a result, we need to type-cast it
// back to all supported implementations.
// This status is particularly useful when a span is not free to collect, or
// has to turn on some special configs. One example is Git Trace2. It's a
// built-in observability tool that provides a deeper look into Git processes.
// Enabling this feature is not expensive, but not cheap either. We don't want
// to enable it for all Git processes. So, it makes sense to collect such data
// when the parent span is sampled.
func IsSampled(span opentracing.Span) bool {
	spanContext := span.Context()

	if jaegerContext, ok := spanContext.(jaeger.SpanContext); ok {
		return jaegerContext.IsSampled()
	}

	if lightstepContext, ok := spanContext.(lightstep.SpanContext); ok {
		return lightstepContext.Sampled == "true"
	}

	// Other non-exported context with conditional build tags. They must
	// implement samplingChecker interface.
	if spanSampler, ok := spanContext.(samplingChecker); ok {
		return spanSampler.IsSampled()
	}

	return false
}

// samplingChecker is an interface consisting of one function that returns
// the sampling status. Some implementations follow opentracing. Labkit doesn't
// need to convert. Some others, such as stackdriver, implement opentracing
// wrappers. Those wrappers are internal, and conditional built when receiving
// corresponding build tag. As a result, such wrappers are not available for
// casting in IsSampled function above.
// Such implementations have to take a detour by implementing this interface.
type samplingChecker interface {
	IsSampled() bool
}
