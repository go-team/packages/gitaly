package tracing

import (
	"github.com/opentracing/opentracing-go"
	"gitlab.com/gitlab-org/labkit/tracing/impl"
)

// IsSampled returns the sampling status (true/false) of a span. This function
// wraps around the actual implementation in `impl` packet. Technically, we don't
// need this wrapper, but the `impl` package contains detailed implementation.
// Most consumers import `gitlab.com/gitlab-org/labkit/tracing`
func IsSampled(span opentracing.Span) bool {
	return impl.IsSampled(span)
}
