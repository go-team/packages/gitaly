package log

import (
	"io"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestAccessLogFormatter_Format(t *testing.T) {
	discardLogger := log.New()
	discardLogger.Out = io.Discard

	tests := []struct {
		name  string
		entry *log.Entry
		want  string
	}{
		{
			"blank",
			discardLogger.WithField("blank", ""),
			"-  - - [2018/01/07:00:00:00 +0000] \"  \" 0 0 \"\" \"\" 0\n",
		},
		{
			"full",
			discardLogger.WithFields(log.Fields{
				httpHostField:               "gitlab.com",
				httpRemoteIPField:           "127.0.0.1",
				httpRequestMethodField:      "GET",
				httpURIField:                "/",
				httpProtoField:              "HTTP/1.1",
				httpResponseStatusCodeField: 200,
				httpResponseSizeField:       100,
				httpRequestReferrerField:    "http://localhost",
				httpUserAgentField:          "Mozilla/1.0",
				requestDurationField:        5,
			}),
			"gitlab.com 127.0.0.1 - - [2018/01/07:00:00:00 +0000] \"GET / HTTP/1.1\" 200 100 \"http://localhost\" \"Mozilla/1.0\" 5\n",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &combinedAccessLogFormatter{clock: &stubClock{time.Unix(1515283200, 0).UTC()}}

			got, err := f.Format(tt.entry)
			if err != nil {
				t.Errorf("AccessLogFormatter.Format() error = %v", err)
				return
			}

			assert.Equal(t, tt.want, string(got))
		})
	}
}
